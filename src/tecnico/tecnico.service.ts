import { Injectable } from '@nestjs/common';
import {Tecnico} from "../entities/tecnico";
import {getConnection, getManager} from "typeorm";
import {TecnicoDto} from "./tecnicoDto";

@Injectable()
export class TecnicoService {
    async getTecnicos(): Promise<Tecnico[]> {
        return  getManager()
            .createQueryBuilder(Tecnico, "t")
            .select(['t.slug','t.nombre','t.apellidoPat','t.apellidoMat','t.cargo'])
            .getMany();
    }
    async getTecnicoBySlug(slug): Promise<Tecnico> {
        return getManager()
            .createQueryBuilder(Tecnico, "t")
            .where('t.slug = :slug',{slug: slug})
            .select(['t.slug','t.nombre','t.apellidoPat','t.apellidoMat','t.cargo'])
            .getOne();
    }
    async getTecnicoById(slug): Promise<Tecnico> {
        return getManager()
            .createQueryBuilder(Tecnico, "t")
            .where('t.slug = :slug',{slug: slug})
            .getOne();
    }
    async deleteTecnico(slug) {
        return await getConnection()
            .createQueryBuilder()
            .delete()
            .from(Tecnico)
            .where("slug = :slug", { slug: slug })
            .execute();
    }
    async addTecnico(tecnico: TecnicoDto) {
        return getManager().transaction(async transactionalEntityManager => {
            let tec: Tecnico = new Tecnico();
            tec.nombre = tecnico.nombre;
            tec.apellidoPat = tecnico.apellidoPat;
            tec.apellidoMat = tecnico.apellidoMat;
            tec.cargo = tecnico.cargo;
            tec.slug = 'jsdksjdkjskdjskdjksd';
            await transactionalEntityManager.save(tec);
        });
    }
    async updateTecnico(tecnico: Tecnico) {
        console.log('TECNICO',tecnico.id);
        return getManager().transaction(async transactionalEntityManager => {
            let tec: Tecnico = await this.getTecnicoById(tecnico.id);
            console.log('TEC', tec);
            tec.nombre = tecnico.nombre;
            tec.apellidoPat = tecnico.apellidoPat;
            tec.apellidoMat = tecnico.apellidoMat;
            tec.cargo = tecnico.cargo;
            await transactionalEntityManager.save(tec);
        });
    }
}
