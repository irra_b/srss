import {ApiModelProperty} from "@nestjs/swagger";

export class TecnicoDto {
    @ApiModelProperty()
    id: number;
    @ApiModelProperty()
    nombre: string;
    @ApiModelProperty()
    apellidoPat: string;
    @ApiModelProperty()
    apellidoMat: string;
    @ApiModelProperty()
    cargo: string;
    @ApiModelProperty()
    slug: string;
}