import {Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Res} from '@nestjs/common';
import {TecnicoService} from "./tecnico.service";
import {Tecnico} from "../entities/tecnico";
import {ApiResponse} from "@nestjs/swagger";
import {TecnicoDto} from "./tecnicoDto";

@Controller('tecnico')
export class TecnicoController {
    constructor(private t: TecnicoService) {}


    @Get()
    @ApiResponse({ status: 200, description: ''})
    @ApiResponse({ status: 401, description: ''})
    getTecnicos(@Res() res) {
        this.t.getTecnicos().then( response => {
            res.status(HttpStatus.OK).json(response);
        }, err => {
            res.status(HttpStatus.NOT_FOUND).json(err);
        });
    }
    @Get(':slug')
    @ApiResponse({ status: 201, description: 'The record has been successfully created.'})
    @ApiResponse({ status: 401, description: 'Nor found.'})
    getTecnico(@Param() param, @Res() res) {
        console.log(param);
        this.t.getTecnicoBySlug(param.slug).then( response => {
            res.status(HttpStatus.OK).json(response);
        }, err => {
            res.status(HttpStatus.NOT_FOUND).json(err);
        });
    }
    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.'})
    @ApiResponse({ status: 403, description: 'Forbidden.'})
    postTecnico(@Body() param: TecnicoDto, @Res() res) {
        this.t.addTecnico(param).then( response => {
            res.status(HttpStatus.OK).json(param);
        }, err => {
            res.status(HttpStatus.NOT_FOUND).json(err);
        });
    }
    @Put()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.'})
    @ApiResponse({ status: 403, description: 'Forbidden.'})
    putTecnico(@Body() param: Tecnico, @Res() res) {
        this.t.updateTecnico(param).then( response => {
            console.log(response);
            res.status(HttpStatus.OK).json(param);
        }, err => {
            res.status(HttpStatus.NOT_FOUND).json(err);
        });
    }
    @Delete(':slug')
    @ApiResponse({ status: 201, description: 'The record has been successfully created.'})
    @ApiResponse({ status: 403, description: 'Forbidden.'})
    deleteTecnico(@Param() param: Tecnico, @Res() res) {
        this.t.deleteTecnico(param.slug).then( response => {
            console.log(response);
            res.status(HttpStatus.OK).json(param);
        }, err => {
            res.status(HttpStatus.NOT_FOUND).json(err);
        });
    }
}
