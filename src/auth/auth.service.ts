import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import {UserService} from "../user/user.service";
import {JwtPayload} from "./jwt-payload.interface";
import * as jwt from 'jsonwebtoken';
import { Observable } from 'rxjs';
import {Payload} from "./payload";
import {Usuario} from "../entities/usuario";

@Injectable()
export class AuthService implements CanActivate{
    constructor(
        private readonly usersService: UserService,
    ) {}

    canActivate(
      context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const request = context.switchToHttp().getRequest();
        let token = request.headers.authorization.replace("Bearer ",  "");
        return this.verify(token).then(data => {
            return true;
        }, err => {
            return false;
        });
    }
    async getDataToken(token: string): Promise<Payload> {
        return new Promise((resolve, reject) => {
            jwt.verify(token, 'secret-key', (err, decoded) => {
                if ( err == null ) {
                    resolve(decoded);
                } else {
                    reject('Token invalido');
                }
            });
        });
    }
    async verify(token): Promise<string> {
        return new Promise((resolve, reject) => {
            jwt.verify(token, 'secret-key', (err, decoded) => {
                if ( err == null ) {
                    resolve('valido');
                } else {
                    reject('Token invalido');
                }
            });
        });
    }
    async auth(user: string, password: string): Promise<string> {
        let usuario: Usuario;
        await this.usersService.findOne(user).then((data: Usuario) => {
            usuario = data;
        });
        return await new Promise((resolve, reject) => {
            if ( usuario === undefined ) {
                reject(Error('Usuario y/o contraseña invalidos'));
            } else {
                this.usersService.comparePassword(password, usuario.password).then((data) => {
                    if (data) {
                        const usr: JwtPayload = { email: usuario.email, id: usuario.id, username: usuario.username, rol: usuario.rol };
                        const payload: Payload = {
                            exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24),
                            data: usr
                        };
                        jwt.sign(payload, 'secret-key',(a, b) => {
                            if ( a == null ) {
                                resolve(b);
                            } else {
                                resolve(a);
                            }
                        });

                    } else {
                        reject(Error('Usuario y/o contraseña invalidos'));
                    }
                });
            }
        });
    }
}
