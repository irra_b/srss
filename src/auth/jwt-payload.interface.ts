export interface JwtPayload {
    email: string;
    id: number;
    username: string;
    rol: string;
}
