import {Body, Controller, Get, HttpStatus, Param, Post, Put, Req, Res, Headers} from '@nestjs/common';
import {AppService} from "../app.service";
import {AuthService} from "./auth.service";
import {JwtPayload} from "./jwt-payload.interface";
import {Login} from "./login.class";
import {ApiBadRequestResponse, ApiNotAcceptableResponse, ApiNotFoundResponse, ApiOkResponse} from "@nestjs/swagger";
import {Auth} from "./auth.decorator";

@Controller('auth')
export class AuthController {
    constructor(private readonly user: AuthService, private mailer: AppService) {}
    @Post()
    @ApiOkResponse({ description: "{token:'...'}"})
    @ApiBadRequestResponse({ description: 'No llenaste los campo usuario y/o contraseña'})
    @ApiNotFoundResponse({ description: 'Usuario y/o contraseña invalidos'})
    login(@Body() req: Login, @Res() res) {
        try{
            const username: string = req.username;
            const password: string = req.password;
            if (username === undefined || password === undefined) {
                res.status(HttpStatus.BAD_REQUEST).json({message: 'No llenaste los campo usuario y/o contraseña',
                errors: [{
                    message: 'usuario y/o contraseña undefined',
                    code: HttpStatus.NOT_FOUND,
                    field: 'username y/o password'
                }]});
            }
            this.user.auth(username, password).then((data) => {
                res.status(HttpStatus.OK).json({token: data});
            }, err => {
                res.status(HttpStatus.NOT_FOUND).json( {message: err.message,
                    errors: [{
                        message: err.message,
                        code: HttpStatus.NOT_FOUND,
                        field: 'username y/o password'
                    }] });
            });
        } catch (e) {
            res.status(HttpStatus.BAD_REQUEST).json(e);
        }
    }
    @Get()
    @ApiOkResponse({ description: "{message:'...'}"})
    @ApiNotAcceptableResponse({ description: "{message:'message'}"})
    @ApiBadRequestResponse({ description: "{message: 'No token in header'}"})
    verify(@Headers() headers, @Res() res) {
        try{
            let token = headers.authorization.replace('Bearer ','');
            this.user.verify(token).then((data) => {
                res.status(HttpStatus.OK).json({message: data});
            },err => {
                res.status(HttpStatus.NOT_ACCEPTABLE).json({message: err});
            });
        } catch (e) {
            res.status(HttpStatus.BAD_REQUEST).json({message: 'No token in header'});
        }
    }
}
