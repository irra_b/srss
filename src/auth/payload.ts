import {JwtPayload} from "./jwt-payload.interface";

export interface Payload {
    exp: number;
    data: JwtPayload;
    iat?: number;
}