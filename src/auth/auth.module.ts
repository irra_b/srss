import { Module } from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import {AuthController} from "./auth.controller";
import {AuthService} from "./auth.service";
import {AppService} from "../app.service";
import {UserService} from "../user/user.service";
import { AuthGuard } from './auth.guard';
import {Usuario} from "../entities/usuario";

@Module({
    imports: [],
    providers: [AuthService, UserService, AppService],
    controllers: [AuthController],
    exports: [AuthService]
})
export class AuthModule {}
