import {Inject, Injectable} from '@nestjs/common';

@Injectable()
export class AppService {
  constructor(@Inject('MailerProvider') private readonly mailerProvider) {

  }
  async sendMail(subject:string, text: string, msg: string, to: string): Promise <void> {
      return await this.mailerProvider.sendMail({
          to: to, // sender address
          from: 'israel.rollano@planificacion.gob.bo', // list of receivers
          subject: subject, // Subject line
          text: text, // plaintext body
          html: msg // HTML body content
      });
  }
}
