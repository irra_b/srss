import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Agenda} from "./agenda";


@Entity("rec_solicitud",{schema:"srssdb" } )
export class RecSolicitud {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:50,
        name:"slug"
        })
    slug:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"nro_reg_interno"
        })
    nroRegInterno:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"fecha_recibido_direccion"
        })
    fechaRecibidoDireccion:string | null;
        

    @Column("time",{ 
        nullable:true,
        name:"hora_recibido_direccion"
        })
    horaRecibidoDireccion:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"referencia"
        })
    referencia:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"remitente"
        })
    remitente:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"cargo"
        })
    cargo:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"telefono"
        })
    telefono:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"fecha_recibido_tics"
        })
    fechaRecibidoTics:string | null;
        

    @Column("time",{ 
        nullable:true,
        name:"hora_recibido_tics"
        })
    horaRecibidoTics:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"origen"
        })
    origen:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"destino/oficina"
        })
    destinoOficina:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"proveido"
        })
    proveido:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:250,
        name:"observaciones"
        })
    observaciones:string | null;
        

   
    @OneToMany(type=>Agenda, agenda=>agenda.recSolicitud,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    agendas:Agenda[];
    
}
