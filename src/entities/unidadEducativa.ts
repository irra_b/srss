import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Agenda} from "./agenda";


@Entity("unidad_educativa",{schema:"srssdb" } )
export class UnidadEducativa {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:50,
        name:"slug"
        })
    slug:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:60,
        name:"unidad_educativa"
        })
    unidadEducativa:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"direccion"
        })
    direccion:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        name:"distrito"
        })
    distrito:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:15,
        name:"lat"
        })
    lat:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:15,
        name:"long"
        })
    long:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:32,
        name:"concatenado_valores"
        })
    concatenadoValores:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"cod_sie"
        })
    codSie:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"turno"
        })
    turno:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"telefono"
        })
    telefono:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:30,
        name:"cant_equipos"
        })
    cantEquipos:string | null;
        

    @Column("tinyint",{ 
        nullable:true,
        name:"prof_comp"
        })
    profComp:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:30,
        name:"internet_prov"
        })
    internetProv:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:200,
        name:"observaciones"
        })
    observaciones:string | null;
        

   
    @OneToMany(type=>Agenda, agenda=>agenda.uniEdu,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    agendas:Agenda[];
    
}
