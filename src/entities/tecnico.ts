import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Asignado} from "./asignado";


@Entity("tecnico",{schema:"srssdb" } )
export class Tecnico {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:50,
        name:"slug"
        })
    slug:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"nombre"
        })
    nombre:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"apellido_pat"
        })
    apellidoPat:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"apellido_mat"
        })
    apellidoMat:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:30,
        name:"cargo"
        })
    cargo:string | null;
        

   
    @OneToMany(type=>Asignado, asignado=>asignado.tecnico,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    asignados:Asignado[];
    
}
