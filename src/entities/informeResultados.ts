import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Agenda} from "./agenda";


@Entity("informe_resultados",{schema:"srssdb" } )
@Index("fk_agenda_id2",["agenda",])
export class InformeResultados {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:50,
        name:"slug"
        })
    slug:string;
        

    @Column("time",{ 
        nullable:true,
        name:"de_hora"
        })
    deHora:string | null;
        

    @Column("time",{ 
        nullable:true,
        name:"a_hora"
        })
    aHora:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:300,
        name:"informe"
        })
    informe:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:250,
        name:"observaciones"
        })
    observaciones:string | null;
        

   
    @ManyToOne(type=>Agenda, agenda=>agenda.informeResultadoss,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'agenda_id'})
    agenda:Agenda | null;

}
