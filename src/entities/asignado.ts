import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Tecnico} from "./tecnico";
import {Agenda} from "./agenda";


@Entity("asignado",{schema:"srssdb" } )
@Index("fk_tecnico_id",["tecnico",])
@Index("fk_agenda_id",["agenda",])
export class Asignado {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

   
    @ManyToOne(type=>Tecnico, tecnico=>tecnico.asignados,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'tecnico_id'})
    tecnico:Tecnico | null;


   
    @ManyToOne(type=>Agenda, agenda=>agenda.asignados,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'agenda_id'})
    agenda:Agenda | null;

}
