import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {RecSolicitud} from "./recSolicitud";
import {UnidadEducativa} from "./unidadEducativa";
import {Asignado} from "./asignado";
import {InformeResultados} from "./informeResultados";


@Entity("agenda",{schema:"srssdb" } )
@Index("fk_rec_solicitud_id",["recSolicitud",])
@Index("fk_uni_edu_id",["uniEdu",])
export class Agenda {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:50,
        name:"slug"
        })
    slug:string;
        

    @Column("date",{ 
        nullable:true,
        name:"dia_programado"
        })
    diaProgramado:string | null;
        

    @Column("time",{ 
        nullable:true,
        name:"hora"
        })
    hora:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"contacto"
        })
    contacto:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"cargo"
        })
    cargo:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"telefono"
        })
    telefono:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"turno_servicio"
        })
    turnoServicio:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:250,
        name:"observaciones"
        })
    observaciones:string | null;
        

   
    @ManyToOne(type=>RecSolicitud, rec_solicitud=>rec_solicitud.agendas,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'rec_solicitud_id'})
    recSolicitud:RecSolicitud | null;


   
    @ManyToOne(type=>UnidadEducativa, unidad_educativa=>unidad_educativa.agendas,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'uni_edu_id'})
    uniEdu:UnidadEducativa | null;


   
    @OneToMany(type=>Asignado, asignado=>asignado.agenda,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    asignados:Asignado[];
    

   
    @OneToMany(type=>InformeResultados, informe_resultados=>informe_resultados.agenda,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    informeResultadoss:InformeResultados[];
    
}
