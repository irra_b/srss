import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("usuario",{schema:"srssdb" } )
@Index("IDX_6ccff37176a6978449a99c82e1",["username",],{unique:true})
export class Usuario {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"nombre"
        })
    nombre:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"cargo"
        })
    cargo:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"email"
        })
    email:string;
        

    @Column("text",{ 
        nullable:false,
        name:"foto"
        })
    foto:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"username"
        })
    username:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"password"
        })
    password:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"rol"
        })
    rol:string;
        

    @Column("tinyint",{ 
        nullable:false,
        name:"habilitado"
        })
    habilitado:number;

    @Column("tinyint",{
        nullable:false,
        name:"habilitado2"
    })
    habilitado2:number;
        
}
