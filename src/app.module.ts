import { Module } from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import { UserModule } from './user/user.module';
import {MailerModule} from "@nest-modules/mailer";
import {AuthModule} from "./auth/auth.module";
import { AppController } from './app.controller';
import { TecnicoModule } from './tecnico/tecnico.module';
import { UnidadEducativaModule } from './unidad-educativa/unidad-educativa.module';
@Module({
  imports: [
      TypeOrmModule.forRoot({
        "type": "mysql",
        "host": "localhost",
        "port": 3306,
        "username": "root",
        "password": "123456",
        "database": "srssdb",
        "entities": [__dirname + '/entities/*{.ts,.js}'],
        "synchronize": true,
        "logging": true
      }),
      UserModule,
      AuthModule,
      MailerModule.forRoot({
        transport: {
          host: 'mail.planificacion.gob.bo',
          port: 587,
          secure: false,
          auth: {
            user: 'israel.rollano@planificacion.gob.bo',
            pass: 'Sistemas2015*'
          },
          tls:{
            rejectUnauthorized:false
          }
        },
        defaults: {
          forceEmbeddedImages: false,
          from:'"nest-modules" <irollano@planificacion.gob.bo>',
        }
      }),
      TecnicoModule,
      UnidadEducativaModule,
  ],
  providers: [],
  controllers: [AppController],

})
export class AppModule {}
