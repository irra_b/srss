import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";
import * as compression from 'compression';
import {UserModule} from "./user/user.module";
import {AuthModule} from "./auth/auth.module";
import {TecnicoModule} from "./tecnico/tecnico.module";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
    const options = new DocumentBuilder()
        .setTitle('API')
        .setDescription('API Sistema')
        .setVersion('1.0')
        .setExternalDoc('api auth', '/api/auth')
        .addTag('user')
        .build();
    const document = SwaggerModule.createDocument(app, options, {
        include: [TecnicoModule],
    });
    SwaggerModule.setup('api', app, document);

  app.use(compression());
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
