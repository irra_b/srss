import { Test, TestingModule } from '@nestjs/testing';
import { UnidadEducativaService } from './unidad-educativa.service';

describe('UnidadEducativaService', () => {
  let service: UnidadEducativaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UnidadEducativaService],
    }).compile();

    service = module.get<UnidadEducativaService>(UnidadEducativaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
