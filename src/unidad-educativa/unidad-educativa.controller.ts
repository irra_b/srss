import {Controller, Get, HttpStatus, Res} from '@nestjs/common';
import {UnidadEducativaService} from "./unidad-educativa.service";

@Controller('unidad-educativa')
export class UnidadEducativaController {
    constructor(private ue: UnidadEducativaService) {

    }


    @Get()
    getUnidadesEducativas(@Res() res) {

        this.ue.getUnidadesEducativas().then( response => {
            res.status(HttpStatus.OK).json(response);
        }, err => {
            res.status(HttpStatus.NOT_FOUND).json(err);
        });
    }
}
