import { Test, TestingModule } from '@nestjs/testing';
import { UnidadEducativaController } from './unidad-educativa.controller';

describe('UnidadEducativa Controller', () => {
  let controller: UnidadEducativaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UnidadEducativaController],
    }).compile();

    controller = module.get<UnidadEducativaController>(UnidadEducativaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
