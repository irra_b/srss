import { Injectable } from '@nestjs/common';
import {UnidadEducativa} from "../entities/unidadEducativa";
import {getConnection, getManager} from "typeorm";

@Injectable()
export class UnidadEducativaService {
    async getUnidadesEducativas() {
        return  getManager()
            .createQueryBuilder(UnidadEducativa, "ue")
            .select(['ue.slug','ue.unidadEducativa','ue.direccion','ue.distrito','ue.lat','ue.long','ue.codSie','ue.telefono','ue.telefono','ue.cantEquipos','ue.profComp','ue.internetProv','ue.observaciones'])
            .getMany();
    }
}
