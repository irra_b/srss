import { Module } from '@nestjs/common';
import { UnidadEducativaController } from './unidad-educativa.controller';
import { UnidadEducativaService } from './unidad-educativa.service';

@Module({
  controllers: [UnidadEducativaController],
  providers: [UnidadEducativaService]
})
export class UnidadEducativaModule {}
