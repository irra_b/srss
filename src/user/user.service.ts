import { Injectable } from '@nestjs/common';
import {getConnection, getManager} from "typeorm";
import * as bcrypt from 'bcrypt';
import {Usuario} from "../entities/usuario";

@Injectable()
export class UserService {
    constructor() {}

    async add(u: Usuario) {
        console.log(u);
        let usuario = new Usuario();
        usuario.username = u.username;
        usuario.habilitado = 1;
        usuario.foto = '';
        usuario.email = u.email;
        usuario.cargo = u.cargo;
        usuario.nombre = u.nombre;
        usuario.rol = u.rol;
        await this.getPassword(u.password).then(hash => {
            usuario.password = hash;
        },err => {
            console.log(err);
        });
        return await new Promise((resolve, reject) => {
            getConnection()
                .createQueryBuilder()
                .insert()
                .into(Usuario)
                .values(usuario)
                .execute().then( ok => {
                resolve(ok);
            }, error => {
                reject(error);
            });
        });
    }
    async update(u: Usuario, id) {
        return await getConnection()
            .createQueryBuilder()
            .update(Usuario)
            .set(u)
            .where("id = :id", { id: id })
            .execute();
    }
    async savePhoto(u: Usuario, id) {
        return await getConnection()
            .createQueryBuilder()
            .update(Usuario)
            .set(u)
            .where("id = :id", { id: id })
            .execute();
    }
    async getPassword(password: string): Promise<string> {
            return bcrypt.hash(password, 10);
    }
    async comparePassword(password1: string, password2: string): Promise<string> {
        return bcrypt.compare(password1, password2);
    }
    async getUser(id): Promise<Usuario> {
        return await getManager().createQueryBuilder()
            .select()
            .from(Usuario, "user")
            .where("user.id = :id", { id: id })
            .getOne();
    }
    async findOne(username): Promise<Usuario> {
        return await getManager().createQueryBuilder()
            .select()
            .from(Usuario, "user")
            .where("user.username = :username", { username: username })
            .getOne();
    }

    async getUsers(id): Promise<Usuario[]> {
        return await getConnection()
            .createQueryBuilder(Usuario, "user")
            .select(["user.id",'user.nombre','user.cargo','user.rol','user.username','user.email','user.foto'])
            .where("user.id != :id", { id: id })
            .getMany();
    }
    async getFullUser(id): Promise<Usuario> {
        return null;
    }
/*TEST*/
    async agregar(u: Usuario) {
        const manager = getConnection();
        const queryRunner = manager.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        let response: any = {code: '', message: ''};
            try {
                // execute some operations on this transaction:
                const usuario: Usuario = new Usuario();
                usuario.username = u.username;
                usuario.habilitado = 1;
                usuario.foto = u.foto;
                usuario.email = u.email;
                usuario.cargo = u.cargo;
                usuario.nombre = u.nombre;
                await this.getPassword(u.password).then(hash => {
                    usuario.password = hash;
                });
                await queryRunner.manager.save(usuario);
                console.log('commit');
                await queryRunner.commitTransaction();
                response = {code: 201, message: 'commit'};

            } catch (err) {
                console.log('rollback', err.message);
                // since we have errors lets rollback changes we made
                await queryRunner.rollbackTransaction();
                response = {code: 400, message: 'rollback', err: err.message};
            } finally {
                // you need to release query runner which is manually created:
                await queryRunner.release();
                console.log('FIN');
                return new Promise((ok, err) => {
                    if(response.code == 201) {
                        ok(response);
                    } else {
                        err(response);
                    }
                });
            }
        //});
    }
}
