import { Body, Controller, Get, HttpStatus, Param, Post, Put, Req, Res, UseGuards } from '@nestjs/common';
import {UserService} from "./user.service";
import {AppService} from "../app.service";
import {ApiCreatedResponse, ApiForbiddenResponse, ApiImplicitParam, ApiOkResponse, ApiResponse} from "@nestjs/swagger";
import { AuthService } from '../auth/auth.service';
import {JwtPayload} from "../auth/jwt-payload.interface";
import {Payload} from "../auth/payload";
import {Usuario} from "../entities/usuario";

@Controller('user')
export class UserController {
    constructor(private readonly user: UserService, private mailer: AppService, private auth: AuthService) {}

    @Get('all')
    @ApiResponse({ status: 201, description: 'The record has been successfully created.'})
    @ApiResponse({ status: 403, description: 'Forbidden.'})
    async getAllUsers(@Req() req, @Res() res) {

        let token = req.headers.authorization;
        token = token.replace('Bearer ','');
        let u: JwtPayload;
        await this.auth.getDataToken(token).then((data: Payload) => {
            u = data.data;
        });
            this.user.getUsers(u.id).then((r) => {
                res.status(HttpStatus.OK).json(r);
            }, err => {
                res.status(HttpStatus.NOT_FOUND).json({ message: err.message });
            });
    }

    @Get()
    @UseGuards(AuthService)
    getUsers(@Req() req, @Res() res) {
        this.getUserId(req).then((data: Payload) => {
            this.user.getUser(data.data.id).then((r) => {
                res.status(HttpStatus.OK).json(r[0]);
            }, err => {
                res.status(HttpStatus.NOT_FOUND).json({ message: err.message });
            });
        }, error => {
            res.status(HttpStatus.NOT_FOUND).json({ message: error.message });
        });
    }

    @Get('verify/:password')
    @UseGuards(AuthService)
    async isValidPassword(@Param() params, @Req() req, @Res() res) {
        let token = req.headers.authorization;
        token = token.replace('Bearer ','');
        let u: JwtPayload;
        await this.auth.getDataToken(token).then((data: Payload) => {
            u = data.data;
        });
        this.user.getFullUser(u.id).then(data => {
            this.user.comparePassword(params.password, data.password).then( resp => {
                res.status(HttpStatus.OK).json(resp);
            }, error => {
                res.status(HttpStatus.NOT_FOUND).json(error);
            });
        }, err => {
            res.status(HttpStatus.NOT_FOUND).json(err);
        });
    }

    @Post()
    @ApiCreatedResponse({ description: 'El usuario fue creado exitosamente.'})
    @ApiForbiddenResponse({ description: 'No se pudo crear el usuario.'})
    @UseGuards(AuthService)
    addUser(@Body() req: Usuario, @Res() res) {
        this.user.add(req).then(() => {
            this.mailer.sendMail('Welcome','Hola','<h1>hola '+req.nombre+'</h1>', req.email).then(data=>{
                console.log(data);
            },err=>{
                console.log(err);
            });
            res.status(HttpStatus.CREATED).json({message: 'OK'});
        }, err => {
            res.status(HttpStatus.NOT_FOUND).json({ message: err.message });
        });

    }

    @Put()
    @ApiOkResponse({ description: 'El usuario fue modificado exitosamente.'})
    @ApiForbiddenResponse({ description: 'No se pudo modificar el usuario.'})
    @UseGuards(AuthService)
    async updateUser(@Body() req: Usuario, @Res() res, @Req() request) {
        console.log('entro ()');
        let token = request.headers.authorization;
        token = token.replace('Bearer ','');
        let u: JwtPayload;
        await this.auth.getDataToken(token).then((data: Payload) => {
            u = data.data;
        });
        this.user.update(req, u.id).then(() => {
            res.status(HttpStatus.OK).json({ message: 'OK' });
        }, err => {
            res.status(HttpStatus.NOT_FOUND).json({ message: err.message });
        });

    }

    @Put('profile')
    @ApiOkResponse({ description: 'Foto de perfin modificada exitosamente'})
    @ApiForbiddenResponse({ description: 'No se pudo modificar la foto.'})
    @UseGuards(AuthService)
    async savePhoto(@Body() req: Usuario, @Res() res, @Req() request) {
        console.log('entro profile');
        let token = request.headers.authorization;
        token = token.replace('Bearer ','');
        let u: JwtPayload;
        await this.auth.getDataToken(token).then((data: Payload) => {
            u = data.data;
        });
        this.user.savePhoto(req, u.id).then(() => {
            res.status(HttpStatus.OK).json({ msg: 'OK' });
        }, err => {
            res.status(HttpStatus.NOT_FOUND).json({ msg: err.message });
        });

    }

    @Put('password')
    @ApiOkResponse({ description: 'El usuario fue modificado exitosamente.'})
    @ApiForbiddenResponse({ description: 'No se pudo modificar el usuario.'})
    @UseGuards(AuthService)
    async updatePasswordUser(@Body() req: Usuario, @Req() request, @Res() res) {
        let token = request.headers.authorization;
        token = token.replace('Bearer ','');
        let u: JwtPayload;
        await this.auth.getDataToken(token).then((data: Payload) => {
            u = data.data;
        });
        this.user.update(req, u.id).then(() => {
            res.status(HttpStatus.OK).json({ message: 'OK' });
        }, err => {
            res.status(HttpStatus.NOT_FOUND).json({ message: err.message });
        });

    }

    @Put(':id')
    @ApiImplicitParam({ name: 'id' })
    @ApiOkResponse({ description: 'El usuario fue modificado exitosamente.'})
    @ApiForbiddenResponse({ description: 'No se pudo modificar el usuario.'})
    @UseGuards(AuthService)
    async updateSelectedUser(@Body() req: Usuario, @Res() res, @Param() params, @Req() request) {
        console.log('entro :id');
        this.user.update(req, params.id).then(() => {
            res.status(HttpStatus.OK).json({ message: 'OK' });
        }, err => {
            res.status(HttpStatus.NOT_FOUND).json({ message: err.message });
        });

    }

    async getUserId(req): Promise<Payload> {
        let token = req.headers.authorization;
        token = token.replace('Bearer ','');
        return this.auth.getDataToken(token);
    }

    @Post('agregar')
    @ApiCreatedResponse({ description: 'El usuario fue creado exitosamente.'})
    @ApiForbiddenResponse({ description: 'No se pudo crear el usuario.'})
    addUser2(@Body() req: Usuario, @Res() res) {
        this.user.agregar(req).then( data => {
            res.status(HttpStatus.OK).json({ msg: data });
        }, err => {
            res.status(HttpStatus.NOT_FOUND).json({ msg: err });
        });
    }
}
