import { Module } from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import {UserService} from "./user.service";
import {UserController} from "./user.controller";
import {AppService} from "../app.service";
import {AuthService} from "../auth/auth.service";
import {Usuario} from "../entities/usuario";

@Module({
    imports: [],
    providers: [UserService, AppService, AuthService],
    controllers: [UserController],
})
export class UserModule {}
